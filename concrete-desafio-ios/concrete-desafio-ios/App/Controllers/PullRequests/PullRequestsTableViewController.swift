//
//  PullRequestsTableViewController.swift
//  concrete-desafio-ios
//
//  Created by Bruno Pereira dos Santos on 12/01/2018.
//  Copyright © 2018 Bruno. All rights reserved.
//

import UIKit

class PullRequestsTableViewController: UITableViewController {

    fileprivate var pullRequests: [PullRequest] = []
    fileprivate let alert = UIAlertController(title: nil, message: "Carregando...", preferredStyle: .alert)
    var owner: String = ""
    var repository: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UINib(nibName: "PullRequestsTableViewCell", bundle: nil), forCellReuseIdentifier: "PullRequestsCell")
        setupAlertLoading()

        self.navigationItem.title = repository
        self.requestPullRequests()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pullRequests.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: "PullRequestsCell", for: indexPath) as? PullRequestsTableViewCell else {
                return UITableViewCell()
        }

        let pullRequest = pullRequests[indexPath.row]

        cell.name.text = pullRequest.title
        cell.about.text = pullRequest.body

        let url = URL(string: pullRequest.user.avatarUrl)
        if let url = url {
            cell.avatar.sd_setImage(with: url, completed: nil)
        }

        cell.login.text = pullRequest.user.login
        cell.userName.text = pullRequest.user.type

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let pullRequest = pullRequests[indexPath.row]
        if let url = URL(string: pullRequest.htmlUrl) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}

extension PullRequestsTableViewController {

    fileprivate func requestPullRequests() {

        present(alert, animated: true) {
            PullRequest.getPullRequests(owner: self.owner, repository: self.repository) { (pullRequests) in
                self.alert.dismiss(animated: true, completion: {

                    if pullRequests.count == 0 {
                        self.showNotFoundAlert()
                        return
                    }

                    self.pullRequests = pullRequests
                    self.tableView.reloadData()
                })
            }
        }
    }

    fileprivate func setupAlertLoading() {

        alert.view.tintColor = UIColor.black
        let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(
            frame: CGRect(x: 10, y: 5, width: 50, height: 50)) as UIActivityIndicatorView
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        loadingIndicator.startAnimating();
        alert.view.addSubview(loadingIndicator)
    }

    fileprivate func showNotFoundAlert() {

        let notFoundAlert = UIAlertController(title: nil, message: "Nenhum pull request encontrado.", preferredStyle: .alert)

        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { UIAlertAction in
            self.navigationController?.popViewController(animated: true)
        }

        notFoundAlert.addAction(okAction)
        present(notFoundAlert, animated: true, completion: nil)
    }
}
