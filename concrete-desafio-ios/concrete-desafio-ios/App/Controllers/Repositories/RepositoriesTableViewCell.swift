//
//  RepositoriesTableViewCell.swift
//  concrete-desafio-ios
//
//  Created by Bruno Pereira dos Santos on 13/01/2018.
//  Copyright © 2018 Bruno. All rights reserved.
//

import UIKit

class RepositoriesTableViewCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var about: UILabel!
    @IBOutlet weak var forks: UILabel!
    @IBOutlet weak var stars: UILabel!
    
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var login: UILabel!
    @IBOutlet weak var userName: UILabel!
}
