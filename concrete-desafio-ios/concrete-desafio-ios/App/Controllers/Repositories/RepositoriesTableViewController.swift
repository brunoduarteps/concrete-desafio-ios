//
//  RepositoriesTableViewController.swift
//  concrete-desafio-ios
//
//  Created by Bruno Pereira dos Santos on 12/01/2018.
//  Copyright © 2018 Bruno. All rights reserved.
//

import UIKit
import SDWebImage

class RepositoriesTableViewController: UITableViewController {

    fileprivate var repositories: [Repository] = []
    fileprivate var page = 0
    fileprivate var selectedRepository: Repository?
    fileprivate let alert = UIAlertController(title: nil, message: "Carregando...", preferredStyle: .alert)
    fileprivate let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UINib(nibName: "RepositoriesTableViewCell", bundle: nil), forCellReuseIdentifier: "RepositoryCell")
        setupAlertLoading()

        self.requestRepositories()
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositories.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: "RepositoryCell", for: indexPath) as? RepositoriesTableViewCell else {
                return UITableViewCell()
        }

        let repository = repositories[indexPath.row]

        cell.name.text = repository.name
        cell.about.text = repository.description
        cell.forks.text = String(repository.forksCount)
        cell.stars.text = String(repository.starsCount)

        let url = URL(string: repository.owner.avatarUrl)
        if let url = url {
            cell.avatar.sd_setImage(with: url, completed: nil)
        }

        cell.login.text = repository.owner.login
        cell.userName.text = repository.owner.type

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedRepository = repositories[indexPath.row]
        self.performSegue(withIdentifier: "ShowPullRequests", sender: self)
    }

    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (indexPath.row == repositories.count - 1) {
            requestRepositories()
        }
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowPullRequests" {
            let controller = segue.destination as? PullRequestsTableViewController
            guard let selectedRepository = selectedRepository else { return }
            controller?.owner = selectedRepository.owner.login
            controller?.repository = selectedRepository.name
        }
    }
}

extension RepositoriesTableViewController {

    fileprivate func requestRepositories() {

        if presentedViewController != nil { return }

        present(alert, animated: true) {
            self.page += 1
            Repository.getRepositoriesIn(page: self.page) { (repositories) in
                self.alert.dismiss(animated: true, completion: {
                    if repositories.count == 0 {
                        return
                    }

                    self.repositories.append(contentsOf: repositories)
                    self.tableView.reloadData()
                })
            }
        }
    }

    fileprivate func setupAlertLoading() {

        alert.view.tintColor = UIColor.black
        let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(
            frame: CGRect(x: 10, y: 5, width: 50, height: 50)) as UIActivityIndicatorView
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        loadingIndicator.startAnimating()
        alert.view.addSubview(loadingIndicator)
    }

    fileprivate func setupSpinner() {

        spinner.startAnimating()
        spinner.frame = CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 44)
        self.tableView.tableFooterView = spinner;
    }
}
