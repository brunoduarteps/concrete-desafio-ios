//
//  Repositories.swift
//  concrete-desafio-ios
//
//  Created by Bruno Pereira dos Santos on 13/01/2018.
//  Copyright © 2018 Bruno. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

enum RepositoryError: Error {
    case requestError
}

struct Repository: Codable {
    
    let name: String
    let description: String
    let starsCount: Int
    let forksCount: Int
    let owner: User

    enum CodingKeys: String, CodingKey {
        case name
        case description
        case starsCount = "stargazers_count"
        case forksCount = "forks"
        case owner
    }
}

extension Repository {

    static func getRepositoriesIn(page: Int, completion: ((_ repositories: [Repository]) -> Void)?){

        let url = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=" + String(page)
        Alamofire.request(url).responseJSON { response in

            if response.error != nil { completion?([]) }

            do {
                
                guard let data = response.data else { throw RepositoryError.requestError }
                let json = JSON(data)
                let repositories = try JSONDecoder().decode([Repository].self, from: json["items"].rawData())
                completion?(repositories)

            } catch {
                completion?([])
            }
        }
    }
}
