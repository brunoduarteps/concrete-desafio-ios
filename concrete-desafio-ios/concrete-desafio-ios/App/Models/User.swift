//
//  owner.swift
//  concrete-desafio-ios
//
//  Created by Bruno Pereira dos Santos on 13/01/2018.
//  Copyright © 2018 Bruno. All rights reserved.
//

import Foundation

struct User: Codable {

    let login: String
    let avatarUrl: String
    let type: String

    enum CodingKeys: String, CodingKey {
        case login
        case avatarUrl = "avatar_url"
        case type
    }
}
