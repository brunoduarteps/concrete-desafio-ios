//
//  PullRequests.swift
//  concrete-desafio-ios
//
//  Created by Bruno Pereira dos Santos on 13/01/2018.
//  Copyright © 2018 Bruno. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

enum PullRequestError: Error {
    case requestError
}

struct PullRequest: Codable {
    
    let htmlUrl: String
    let title: String
    let body: String
    let user: User

    enum CodingKeys: String, CodingKey {
        case htmlUrl = "html_url"
        case title
        case body
        case user
    }
}

extension PullRequest {

    static func getPullRequests(
        owner: String,
        repository: String,
        completion: ((_ pullRequests: [PullRequest]) -> Void)?){

        let url = "https://api.github.com/repos/\(owner)/\(repository)/pulls"
        Alamofire.request(url).responseJSON { response in

            if response.error != nil { completion?([]) }

            do {
                
                guard let data = response.data else { throw PullRequestError.requestError }
                let pullRequests = try JSONDecoder().decode([PullRequest].self, from: data)
                completion?(pullRequests)

            } catch {
                completion?([])
            }
        }
    }
}
